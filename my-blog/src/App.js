import React from "react";
import AppBarView from "./components/AppBarView";
import { Provider } from "react-redux";
import ListView from "./components/ListView";
import Login from "./components/Login";
import store from "./redux/store";
import useUser from "./hooks/useUser";

const App = () => {
  const { userName } = useUser();
  return (
    <>
      <Provider store={store}>
        <AppBarView />
        {userName ? <ListView /> : <Login />}
      </Provider>
    </>
  );
};

export default App;
