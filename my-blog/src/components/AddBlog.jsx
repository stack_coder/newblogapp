import React, { useState } from "react";
import useBlogs from "../hooks/useBlogs";
import BlogForm from "./BlogForm";

const AddBlog = ({ openModal, open, setOpen, create, id }) => {
  console.log("edit", id);

  const { blogs, createBlog, editBlog } = useBlogs();

  const getData = blogs?.find((item) => item._id === id);

  const [state, setState] = useState({
    title: "" || getData?.title,
    description: "" || getData?.description,
  });

  const { title, description } = state;

  const handleClose = () => setOpen(false);

  const handleOpen = () => setOpen(true);

  const onChange = (name, value) => {
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const onSubmit = () => {
    if (id) {
      editBlog(state, id);
    } else {
      createBlog(title, description);
      setState({ title: "", description: "" });
    }
    handleClose();
  };
  return (
    <BlogForm
      openModal={openModal}
      open={open}
      setOpen={setOpen}
      create={create}
      id={id}
      state={state}
      onChange={onChange}
      onSubmit={onSubmit}
      handleClose={handleClose}
      handleOpen={handleOpen}
    />
  );
};

export default AddBlog;
