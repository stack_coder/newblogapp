import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React from "react";
import useUser from "../hooks/useUser";
import AddBlog from "./AddBlog";
import refreshPage from "../helpers/refresh";

const AppBarView = ({ username }) => {
  const [open, setOpen] = React.useState(false);
  const { userName } = useUser();
  const openModal = () => {
    setOpen(true);
  };
  const logout = () => {
    localStorage.removeItem("email");
    refreshPage();
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        position="static"
        style={{
          backgroundColor: "lightblue",
          fontSize: "30px",
        }}
      >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          ></IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            My Blog {username}
          </Typography>

          {userName ? (
            <>
              <Button>
                <AddBlog
                  openModal={openModal}
                  open={open}
                  setOpen={setOpen}
                  create="Create Blog"
                />
              </Button>
              <Button onClick={logout} color="inherit">
                LogOut
              </Button>
            </>
          ) : null}
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default AppBarView;
