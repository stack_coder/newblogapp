import { Button, Grid, Modal, Paper, TextField } from "@material-ui/core";

const BlogForm = ({
  open,
  create,
  handleClose,
  state,
  handleOpen,
  onChange,
  onSubmit,
}) => {
  console.log({});

  const paperStyle = {
    padding: 20,
    height: "40vh",
    width: 300,
    margin: "180px auto",
  };

  const btnstyle = { margin: "30px 0" };

  return (
    <div>
      <Button onClick={handleOpen}>{create}</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Grid>
          <Paper elevation={10} style={paperStyle}>
            <Grid align="center">
              <h2>{create}</h2>
            </Grid>
            <TextField
              label="title"
              placeholder="Enter title"
              fullWidth
              required
              value={state.title}
              onChange={(e) => onChange("title", e.target.value)}
            />
            <TextField
              label="Description"
              placeholder="Enter Description"
              fullWidth
              required
              value={state.description}
              onChange={(e) => onChange("description", e.target.value)}
            />

            <Button
              type="submit"
              color="primary"
              variant="contained"
              style={btnstyle}
              fullWidth
              onClick={onSubmit}
            >
              Submit
            </Button>
          </Paper>
        </Grid>
      </Modal>
    </div>
  );
};

export default BlogForm;
