import { Box, Button, Modal, Typography } from "@material-ui/core";
import * as React from "react";

const style = {
  position: "absolute",
  top: "40%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 320,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

const title = {
  overflow: "hidden",
  textOverflow: "ellipsis",
};
const CardDetail = ({ data }) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  console.log("644456", { data });

  return (
    <div>
      <Button onClick={handleOpen}>Detail</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={style}
          style={{
            backgroundColor: "lightblue",
            marginTop: "60px",
            fontSize: "50px",
            borderRadius: "10px",
          }}
        >
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {data?.title}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }} style={title}>
            {data?.description}
          </Typography>
        </Box>
      </Modal>
    </div>
  );
};

export default CardDetail;
