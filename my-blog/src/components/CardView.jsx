import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import AddBlog from "./AddBlog";
import CardDetail from "./CardDetail";

const CardView = ({ blog }) => {
  const [open, setOpen] = useState(false);

  const openModal = () => {
    setOpen(true);
  };

  return (
    <Box
      sx={{ minWidth: 275 }}
      style={{
        backgroundColor: "lightblue",
        marginTop: "60px",
        fontSize: "30px",
        borderRadius: "10px",
      }}
    >
      <React.Fragment>
        <CardContent>
          <Typography component="div">{blog?.currentDateTime}</Typography>

          <Typography sx={{ fontSize: 74 }} color="text.secondary" gutterBottom>
            Title
          </Typography>
          <Typography variant="h5" component="div">
            {blog?.title}
          </Typography>
          <Typography sx={{ fontSize: 34 }} color="text.secondary" gutterBottom>
            Description
          </Typography>
          <div
            style={{
              textOverflow: "ellipsis",
              width: "300px",
              overflow: "hidden",
              whiteSpace: "nowrap",
            }}
          >
            <Typography variant="h5" component="div">
              {blog?.description}
            </Typography>
          </div>
        </CardContent>
        <CardActions>
          <Button size="small">
            <AddBlog
              openModal={openModal}
              open={open}
              setOpen={setOpen}
              create="Edit"
              id={blog?._id}
            />
          </Button>
          <Button size="small">
            <CardDetail
              openModal={openModal}
              open={open}
              setOpen={setOpen}
              data={blog}
            />
          </Button>
        </CardActions>
      </React.Fragment>
      <Card variant="outlined"></Card>
    </Box>
  );
};

export default CardView;
