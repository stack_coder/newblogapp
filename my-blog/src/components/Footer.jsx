import {
  AppBar,
  Box,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React from "react";

const useStyles = makeStyles({
  box: {
    width: "70%",
    marginLeft: "12%",
    marginTop: "10%",
    color: "#f5f5f5",
  },
});

const Footer = () => {
  const classes = useStyles();

  return (
    <Box
      className={classes.box}
      position="fixed"
      width="100%"
      sx={{ top: "auto", bottom: 0 }}
    >
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            {/* <MenuIcon /> */}
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Footer
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Footer;
