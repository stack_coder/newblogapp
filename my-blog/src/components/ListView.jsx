import { Grid } from "@material-ui/core";
import React from "react";
import useBlogs from "../hooks/useBlogs";
import CardView from "./CardView";

const ListView = () => {
  const { blogs } = useBlogs();
  console.log("34443", blogs);
  return (
    <Grid container spacing={2}>
      {blogs?.map((item) => (
        <Grid item xs={12} sm={6} md={4}>
          <CardView blog={item} />
        </Grid>
      ))}
    </Grid>
  );
};

export default ListView;
