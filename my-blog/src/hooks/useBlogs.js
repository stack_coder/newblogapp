import { useDispatch, useSelector } from "react-redux";
import getRandomId from "../helpers/getRandomId";
import getBlog from "../redux/action/getBlog";

const useBlogs = () => {
  const blogs = useSelector((state) => state.data);
  const dispatch = useDispatch();

  const createBlog = (title, description) => {
    if (title && description) {
      const blog = {
        _id: getRandomId(),
        title,
        description,
        currentDateTime: Date().toLocaleString(),
      };
      const newBlogs = [...blogs, blog];
      dispatch(getBlog(newBlogs));
    }
  };

  const editBlog = (state, id) => {
    if (state && id) {
      const newblog = blogs.find((item) => item._id === id);

      const updatedblog = {
        ...newblog,
        currentDateTime: Date().toLocaleString(),
        title: state.title,
        description: state.description,
      };

      const updateBlogs = blogs.map((item) => {
        if (item._id === updatedblog._id) {
          const newValue = {
            ...item,
            title: updatedblog.title,
            description: updatedblog.description,
          };
          return newValue;
        }
        return item;
      });
      dispatch(getBlog(updateBlogs));
      return updateBlogs;
    }
  };

  return { blogs, createBlog, editBlog };
};

export default useBlogs;
