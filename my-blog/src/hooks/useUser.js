const useUser = () => {
  const userName = localStorage.getItem("email");
  return { userName };
};

export default useUser;
