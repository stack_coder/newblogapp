import { GET_BLOG } from "../type/blog";

const getBlog = (data) => {
  return {
    type: GET_BLOG,
    payload: data,
  };
};

export default getBlog;
