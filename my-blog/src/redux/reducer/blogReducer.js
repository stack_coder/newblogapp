import { GET_BLOG } from "../type/blog";

const blogReducer = (state = { data: [] }, action) => {
  switch (action.type) {
    case GET_BLOG:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default blogReducer;
